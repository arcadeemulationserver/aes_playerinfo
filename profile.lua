local S = minetest.get_translator("aes_playerinfo")



minetest.register_tool("aes_playerinfo:profile", {

  description = S("Profile"),
  inventory_image = "aesplayerinfo_profile.png",
  groups = {not_in_creative_inventory = 1, oddly_breakable_by_hand = "3"},
  on_place = function() end,
  on_drop = function() end,

  on_use = function(itemstack, user, pointed_thing)
    minetest.chat_send_player(user:get_player_name(), "Coming soon")
  end

})
